﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using RimWorld;
using Verse;
using Verse.Sound;

namespace Thumper
{
    public class Building_Thumper : Building
    {
        [DefOf]
        public static class ThumperDefOf
        {
            public static SoundDef O21_Sound_ThumperHit;

            public static SoundDef O21_Sound_ThumperStartup;

            public static SoundDef O21_Sound_ThumperShutdown;
        }

        public CompPowerTrader powerComp => this.GetComp<CompPowerTrader>();

        public CompFlickable flickableComp => this.GetComp<CompFlickable>();

        public ThumperStatus status = ThumperStatus.Inactive;

        public int thumpRate = 1200;

        public int thumpTick = 0;

        public int preventedAttacks = 0;

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<int>(ref thumpTick, "thumpTick");
            Scribe_Values.Look<int>(ref preventedAttacks, "preventedAttacks");
        }

        public override void Tick()
        {
            base.Tick();

            if(status == ThumperStatus.Active && !powerComp.PowerOn)
            {
                ShutdownThumper();
            }
            else if(status == ThumperStatus.Inactive && powerComp.PowerOn)
            {
                StartupThumper();
            }

            if (ThumperRunning())
            {
                thumpTick++;
                if (thumpTick >= thumpRate)
                {
                    ThumpNow();
                    thumpTick = 0;
                }
            }
        }

        public bool ThumperRunning()
        {
            if (powerComp.PowerOn)
            {
                return true;
            }
            return false;
        }

        public void ThumpNow()
        {
            CellRect radius = GenAdj.OccupiedRect(this.Position, this.Rotation, new IntVec2(2, 2));
            radius = radius.ExpandedBy(1);
            if (ThumperMod.mod.settings.soundEnabled)
            {
                SoundStarter.PlayOneShot(ThumperDefOf.O21_Sound_ThumperHit, SoundInfo.InMap(this));
            }

            foreach (IntVec3 cell in radius.Cells)
            {
                MoteMaker.ThrowDustPuff(cell, this.Map, 2f);
            }
        }

        public void StartupThumper()
        {
            SoundStarter.PlayOneShot(ThumperDefOf.O21_Sound_ThumperStartup, SoundInfo.InMap(this));
            status = ThumperStatus.Active;
        }

        public void ShutdownThumper()
        {
            SoundStarter.PlayOneShot(ThumperDefOf.O21_Sound_ThumperShutdown, SoundInfo.InMap(this));
            status = ThumperStatus.Inactive;
        }

        public enum ThumperStatus
        {
            Inactive,
            Active
        }
    }
}
