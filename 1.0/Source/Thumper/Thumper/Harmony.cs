﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using RimWorld;
using Verse;

using Harmony;
using System.Reflection;

namespace Thumper
{
    [StaticConstructorOnStartup]
    public static class Harmony
    {
        static Harmony()
        {
            HarmonyInstance harmony = HarmonyInstance.Create("com.o21thumper.rimworld.mod");

            harmony.Patch(AccessTools.Method(typeof(InfestationCellFinder), "GetScoreAt"), new HarmonyMethod(typeof(Harmony), "InfestationCellFinder_GetScoreAt_PreFix"));
            harmony.Patch(AccessTools.Method(typeof(IncidentWorker_Infestation), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
            harmony.Patch(AccessTools.Method(typeof(IncidentWorker_DeepDrillInfestation), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
            
            // Compatibility Patches
            // Better Infestations 1.0
            if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Better Infestations 1.0"))
            {
                BetterInfestations_Compatibility(harmony);
            }
            // Alpha Animals
            if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Alpha Animals"))
            {
                AlphaAnimals_Compatibility(harmony);
            }
            // Alien Vs Predator
            if (LoadedModManager.RunningModsListForReading.Any(x => x.Name == "Alien Vs Predator"))
            {
                AlienVsPredator_Compatibility(harmony);
            }

            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

        // Universally applicable override for incidents.
        public static bool IncidentWorker_UniversalTryExecuteWorker_Prefix(IncidentWorker_Infestation __instance, IncidentParms parms)
        {
            if (!ThumperMod.mod.settings.entitlementEnabled)
            {
                Map map = (Map)parms.target;
                if (ThumperProtects(map))
                {
                    Messages.Message("Infestation attempt detected and repelled by Thumper.", MessageTypeDefOf.PositiveEvent);
                    return false;
                }
            }
            return true;
        }

        public static bool InfestationCellFinder_GetScoreAt_PreFix(IntVec3 cell, Map map, float __result)
        {
            if (ThumperMod.mod.settings.entitlementEnabled)
            {
                foreach (Building building in map.listerBuildings.allBuildingsColonist)
                {
                    bool flag = building is Building_Thumper && cell.InHorDistOf(building.Position, ThumperMod.mod.settings.thumperRange) && (building.GetComp<CompPowerTrader>().PowerOn);
                    if (flag)
                    {
                        __result = 0f;
                        return false;
                    }
                }
            }
            return true;
        }

        //public static Building_Thumper GetClosestThumper(IntVec3 cell, Map map)
        //{
        //    Building_Thumper closestThumper = null;

        //    foreach (Building_Thumper thumper in map.listerBuildings.AllBuildingsColonistOfDef(DefDatabase<ThingDef>.GetNamed("O21_AntiInfestationThumper")).ToList())
        //    {
        //        if (closestThumper == null)
        //        {
        //            closestThumper = thumper;
        //        }
        //        else if (thumper.powerComp.PowerOn && thumper.flickableComp.SwitchIsOn)
        //        {
        //            if (closestThumper != null && cell.DistanceTo(thumper.Position) < cell.DistanceTo(closestThumper.Position))
        //            {
        //                closestThumper = thumper;
        //            }
        //        }
        //    }

        //    return closestThumper;
        //}

        //public static bool DeepDrillInfestationIncidentUtility_GetUsableDeepDrills(Map map, List<Thing> outDrills)
        //{
        //    if (ThumperMod.mod.settings.entitlementEnabled)
        //    {
        //        outDrills.Clear();
        //        List<Thing> list = map.listerThings.ThingsInGroup(ThingRequestGroup.CreatesInfestations);
        //        Faction ofPlayer = Faction.OfPlayer;
        //        for (int i = 0; i < list.Count; i++)
        //        {
        //            Building_Thumper closestThumper = GetClosestThumper(list[i].Position, map);
        //            bool flag = list[i].Position.DistanceTo(closestThumper.Position) < 26f;

        //            if (list[i].Faction == ofPlayer && !flag)
        //            {
        //                if (list[i].TryGetComp<CompCreatesInfestations>().CanCreateInfestationNow)
        //                {
        //                    outDrills.Add(list[i]);
        //                }
        //            }
        //        }
        //        return false;
        //    }
        //    return true;
        //}

        // Check for if there's currently a thumper active on the map.
        public static bool ThumperProtects(Map map)
        {
            if (!map.listerBuildings.AllBuildingsColonistOfClass<Building_Thumper>().ToList().NullOrEmpty())
            {
                foreach (Building_Thumper thumper in map.listerBuildings.AllBuildingsColonistOfDef(DefDatabase<ThingDef>.GetNamed("O21_AntiInfestationThumper")).ToList())
                {
                    if (thumper.powerComp.PowerOn && thumper.flickableComp.SwitchIsOn)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        // Compatibility for Better Infestations 1.0
        public static void BetterInfestations_Compatibility(HarmonyInstance harmony)
        {
            if (ThumperMod.mod.settings.betterInfestations)
            {
                harmony.Patch(AccessTools.Method(typeof(BetterInfestations.IncidentWorker_Infestation), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
                harmony.Patch(AccessTools.Method(typeof(BetterInfestations.IncidentWorker_DeepDrillInfestation), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
                harmony.Patch(AccessTools.Method(typeof(BetterInfestations.IncidentWorker_FoodInfestation), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
            }
        }

        // Compatibility for Alpha Animals
        public static void AlphaAnimals_Compatibility(HarmonyInstance harmony)
        {
            if (ThumperMod.mod.settings.alphaAnimals)
            {
                harmony.Patch(AccessTools.Method(typeof(AlphaBehavioursAndEvents.IncidentWorker_BlackHive), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
            }
        }

        // Compatibility for Alpha Animals
        public static void AlienVsPredator_Compatibility(HarmonyInstance harmony)
        {
            if (ThumperMod.mod.settings.alienVsPredator)
            {
                harmony.Patch(AccessTools.Method(typeof(IncidentWorker_Hivelike), "TryExecuteWorker"), new HarmonyMethod(typeof(Harmony), "IncidentWorker_UniversalTryExecuteWorker_Prefix"));
            }
        }
    }
}
