﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;
using RimWorld;
using Verse;

namespace Thumper
{
    public class ThumperMod : Mod
    {
        public ThumperSettings settings;
        public static ThumperMod mod;

        public ThumperMod(ModContentPack content) : base(content)
        {
            Log.Message(":: O21 Thumper - Version 1.1 ::");
            settings = GetSettings<ThumperSettings>();
            mod = this;
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(inRect);
            listingStandard.Label("Some changes may require a restart for them to take effect.");
            listingStandard.CheckboxLabeled("Thumper Sound Effect", ref settings.soundEnabled, "Whether or not the continuous 'Thump' is played.");
            listingStandard.CheckboxLabeled("Range Mode", ref settings.entitlementEnabled, "If enabled, changes thumper behaviour to prevent in a radius rather than map wide. Deep drill infestations are not able to be prevented by range mode. May not prevent all modded infestations. Doesn't notify of failed infestations.");
            listingStandard.Label("Range - Only applies if Range Mode is enabled.");
            listingStandard.Label("Min: 1, Max: 60, Current: " + settings.thumperRange.ToString());
            settings.thumperRange = listingStandard.Slider((float)Mathf.RoundToInt(settings.thumperRange), 1f, 80f);
            listingStandard.Label("Compatibility Options");
            listingStandard.CheckboxLabeled("Alien Vs Predator", ref settings.alienVsPredator, "Thumper can disable aliens from burrowing in");
            listingStandard.CheckboxLabeled("Alpha Animals", ref settings.alphaAnimals, "Thumper can disable Black Hive");
            listingStandard.CheckboxLabeled("Better Infestations 1.0", ref settings.betterInfestations, "Better Infestations replaces vanilla ones, this is basically just if you picky entitled bitches want vanilla style ones but want the others disabled.");
            listingStandard.End();
            base.DoSettingsWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "Anti-Infestion Thumper";
        }
    }

    public class ThumperSettings : ModSettings
    {
        public bool soundEnabled = true;
        public bool entitlementEnabled = false;

        public float thumperRange = 16f;

        // Compatibility Options
        public bool alphaAnimals = true;
        public bool alienVsPredator = true;
        public bool betterInfestations = true;

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<bool>(ref soundEnabled, "soundEnabled", true);
            Scribe_Values.Look<bool>(ref entitlementEnabled, "entitlementEnabled", false);
            Scribe_Values.Look<float>(ref thumperRange, "thumperRange", 16f);

            // Compatbility Options
            Scribe_Values.Look<bool>(ref alphaAnimals, "alphaAnimals", true);
            Scribe_Values.Look<bool>(ref alienVsPredator, "alienVsPredator", true);
            Scribe_Values.Look<bool>(ref betterInfestations, "betterInfestations", true);
        }
    }
}
